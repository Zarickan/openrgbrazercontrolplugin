# WIP: Razer Control Plugin 

This is a copy and adaptation of the [OpenRGBRazerExtras](https://gitlab.com/OpenRGBDevelopers/OpenRGBRazerExtrasPlugin) plugin, kept here while it is being developed, so it can depend on a modified OpenRGB (which supports the new required features) until those changes are merged. The idea is to eventually merge the functionality into that plugin. 

## What is this?

Plugin to control/configure Razer devices with the same options as in synapse.

Currently suppported:

- Viper Ultimate, wired/wireless
- BlackWidow v3 Mini, wired/wireless
- Huntsman v2 TKL

## How do I install it?

* Download and extract the correct files depending on your system
* Launch OpenRGB
* From the Settings -> Plugins menu, click the "Install plugin" button

