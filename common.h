#pragma once

#include <stdlib.h>
#include <stdint.h>

// Pointer
typedef size_t size;
typedef intptr_t intptr;
typedef uintptr_t uintptr;

// Unsigned
typedef uint8_t   u8;
typedef uint8_t  u08;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

// Signed
typedef int8_t   s8;
typedef int8_t  s08;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;
typedef s8       i8;
typedef s08     i08;
typedef s16     i16;
typedef s32     i32;
typedef s64     i64;

// Floating point
typedef float  f32;
typedef double f64;

// Helper macros
#define For(target) for(auto it : (target))
#define ARRAYSIZE(array) (sizeof(array) / sizeof((array)[0]))