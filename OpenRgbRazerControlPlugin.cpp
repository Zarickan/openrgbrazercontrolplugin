#include <QHBoxLayout>
#include <thread>
#include <chrono>
#include <ctime>
#include <ratio>

#include "hidapi.h"
#include "OpenRgbRazerControlPlugin.h"

#include "RazerDevices.h"
#include "RazerController.h"
#include "RGBController.h"

#include "HuntsmanControlWidget.h"
#include "HuntsmanV2ControlWidget.h"
#include "ViperUltimateControlWidget.h"
#include "BlackWidowV3MiniControlWidget.h"

// ------------------------------------------------------------------------------------------------------------------------------------------------------
// Utilities
// ------------------------------------------------------------------------------------------------------------------------------------------------------


// ------------------------------------------------------------------------------------------------------------------------------------------------------
// Plugin code
// ------------------------------------------------------------------------------------------------------------------------------------------------------
bool OpenRgbRazerControlPlugin::DarkTheme = false;
ResourceManager* OpenRgbRazerControlPlugin::Resources = nullptr;

OpenRGBPluginInfo OpenRgbRazerControlPlugin::GetPluginInfo()
{
    OpenRGBPluginInfo info;
    info.Name          = "Razer Control Plugin";
    info.Description   = "Frederik Madsen (Zarickan)";
    info.Version       = VERSION_STRING;
    info.Commit        = GIT_COMMIT_ID;
    info.URL           = "https://gitlab.com/OpenRGBDevelopers/OpenRgbRazerControlPlugin";
    info.Location      = OPENRGB_PLUGIN_LOCATION_TOP;
    info.Label         = "Razer Control";
    info.TabIconString = "Razer Control";

    info.Icon.load(":/OpenRgbRazerControlPlugin.png");
    info.TabIcon.load(":/OpenRgbRazerControlPlugin.png");

    return info;
}

unsigned int OpenRgbRazerControlPlugin::GetPluginAPIVersion()
{
    return OPENRGB_PLUGIN_API_VERSION;
}

static auto RazerDetectors = std::map<unsigned int, HIDDeviceDetectorBlock>();
static auto RazerControllers = std::vector<RazerController *>();

void OpenRgbRazerControlPlugin::Load(bool dark_theme, ResourceManager* resource_manager)
{
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    // Find all registered detectors for Razer devices
    For(resource_manager->GetHIDDeviceDetectors()) {
        unsigned int vid = (it.address & 0xFFFF0000) >> 16;
        if (vid != RAZER_VID) continue;

        unsigned int pid = it.address & 0x0000FFFF;
        RazerDetectors.emplace(pid, it);
    }

    // Iterate through HID devices and create RazerController instances for the ones we want
    for(auto *info = hid_enumerate(RAZER_VID, 0); info; info = info->next) {
        auto search = RazerDetectors.find(info->product_id);
        if(search != RazerDetectors.end()) {
            auto detector = search->second;
            if(info->interface_number == detector.interface && info->usage_page == detector.usage_page && info->usage == detector.usage) {
                auto *device = hid_open_path(info->path);
                auto *controller = new RazerController(device, NULL, info->path, info->product_id, detector.name);

                RazerControllers.emplace_back(controller);
            }
        }
    }

    // TODO: Register something to react when device list is refreshed

    resource_manager->WaitForDeviceDetection();
    Resources = resource_manager;
    DarkTheme = dark_theme;
}


QWidget* OpenRgbRazerControlPlugin::GetWidget()
{
    auto *widget = new QWidget(nullptr);
    auto *layout = new QHBoxLayout();

    widget->setLayout(layout);

    // TODO: We want to UI to update when we reload the list of razer controllers

    For(RazerControllers) {
        auto device = device_list[it->GetDeviceIndex()];

        // If the device is wireless check that it is present
        if(device->is_wireless && !it->IsWirelessDevicePresent()) {
            continue;
        }

        switch(device->pid) {
            case RAZER_HUNTSMAN_PID:
            case RAZER_HUNTSMAN_ELITE_PID:
            case RAZER_HUNTSMAN_TE_PID:
            case RAZER_HUNTSMAN_MINI_PID:
                layout->addWidget(new HuntsmanControlWidget(widget, it));
                break;

            case RAZER_HUNTSMAN_V2_PID:
            case RAZER_HUNTSMAN_V2_TKL_PID:
                layout->addWidget(new HuntsmanV2ControlWidget(widget, it));
                break;

            case RAZER_BLACKWIDOW_V3_MINI_WIRED_PID:
            case RAZER_BLACKWIDOW_V3_MINI_WIRELESS_PID:
                layout->addWidget(new BlackWidowV3MiniControlWidget(widget, it));
                break;

            case RAZER_VIPER_ULTIMATE_WIRED_PID:
            case RAZER_VIPER_ULTIMATE_WIRELESS_PID:
                layout->addWidget(new ViperUltimateControlWidget(widget, it));
                break;
        }
    }

    return widget;
}

QMenu* OpenRgbRazerControlPlugin::GetTrayMenu()
{
    return NULL;
}

void OpenRgbRazerControlPlugin::Unload()
{
    For(RazerControllers) {
        delete it;
    }
}

OpenRgbRazerControlPlugin::OpenRgbRazerControlPlugin()
{
}

OpenRgbRazerControlPlugin::~OpenRgbRazerControlPlugin()
{
}

