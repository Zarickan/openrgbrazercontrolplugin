#pragma once

#include "OpenRGBPluginInterface.h"
#include "ResourceManager.h"
#include "common.h"

#include <QObject>
#include <QString>
#include <QtPlugin>
#include <QWidget>

class OpenRgbRazerControlPlugin : public QObject, public OpenRGBPluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID OpenRGBPluginInterface_IID)
    Q_INTERFACES(OpenRGBPluginInterface)

public:
    OpenRgbRazerControlPlugin();
    ~OpenRgbRazerControlPlugin();

    OpenRGBPluginInfo   GetPluginInfo() override;
    unsigned int        GetPluginAPIVersion() override;

    void                Load(bool dark_theme, ResourceManager* resource_manager_ptr) override;
    QWidget*            GetWidget() override;
    QMenu*              GetTrayMenu() override;
    void                Unload() override;

    static bool             DarkTheme;
    static ResourceManager* Resources;
};