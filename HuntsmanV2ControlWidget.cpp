#include "HuntsmanV2ControlWidget.h"
#include "ui_HuntsmanV2ControlWidget.h"

#include "RazerController.h"
#include "common.h"

static QString PollingRateNames[] = {
    "125 HZ",
    "250 HZ",
    "500 HZ",
    "1000 HZ",
    "2000 HZ",
    "4000 HZ",
    "8000 HZ",
};
static unsigned char PollingRateValues[] = {
    RAZER_KEYBOARD_POLLING_RATE_125HZ,
    RAZER_KEYBOARD_POLLING_RATE_250HZ,
    RAZER_KEYBOARD_POLLING_RATE_500HZ,
    RAZER_KEYBOARD_POLLING_RATE_1000HZ,
    RAZER_KEYBOARD_POLLING_RATE_2000HZ,
    RAZER_KEYBOARD_POLLING_RATE_4000HZ,
    RAZER_KEYBOARD_POLLING_RATE_8000HZ,
};

static QString SwitchOptimizationNames[] = {
        "Gaming",
        "Typing",
};
static unsigned int SwitchOptimizationValues[] = {
        RAZER_SWITCH_OPTIMIZATION_GAMING,
        RAZER_SWITCH_OPTIMIZATION_TYPING,
};

void HuntsmanV2ControlWidget::UpdateInformation() {
    ready = false;

    ui->GamingModeCheck->setChecked(Controller->GetKeyboardGamingMode());

    // Polling Rate
    unsigned char polling_rate = Controller->GetKeyboardPollingRate();
    for(int i = 0; i < ARRAYSIZE(PollingRateValues); ++i) {
        if(polling_rate == PollingRateValues[i]) {
            ui->PollingRateCombo->setCurrentIndex(i);
            break;
        }
    }

    // Switch optimization
    unsigned int switch_optimization = Controller->GetKeyboardSwitchOptimization();
    for(int i = 0; i < ARRAYSIZE(SwitchOptimizationValues); ++i) {
        if(switch_optimization == SwitchOptimizationValues[i]) {
            ui->SwitchOptimizationCombo->setCurrentIndex(i);
            break;
        }
    }

    ready = true;
}

HuntsmanV2ControlWidget::HuntsmanV2ControlWidget(QWidget *parent, RazerController *controller) :
    QWidget(parent),
    ui(new Ui::HuntsmanV2ControlWidget)
{
    Controller = controller;

    ui->setupUi(this);
    ui->NameValue->setText(QString::fromStdString(controller->GetName()));

    // Polling rates
    for(const auto& polling_rate : PollingRateNames) {
        ui->PollingRateCombo->addItem(polling_rate);
    }

    // Switch optimizations
    for(const auto& switch_optimization : SwitchOptimizationNames) {
        ui->SwitchOptimizationCombo->addItem(switch_optimization);
    }

    UpdateInformation();
    ready = true;
}

HuntsmanV2ControlWidget::~HuntsmanV2ControlWidget()
{
    delete ui;
}

void HuntsmanV2ControlWidget::on_GamingModeCheck_clicked()
{
    if(!ready) return;

    Controller->SetKeyboardGamingMode(ui->GamingModeCheck->isChecked());
    UpdateInformation();
}

void HuntsmanV2ControlWidget::on_PollingRateCombo_currentIndexChanged()
{
    if(!ready) return;

    unsigned char desired_polling_rate = PollingRateValues[ui->PollingRateCombo->currentIndex()];
    Controller->SetKeyboardPollingRate(desired_polling_rate);
    UpdateInformation();
}

void HuntsmanV2ControlWidget::on_SwitchOptimizationCombo_currentIndexChanged()
{
    if(!ready) return;

    unsigned int desired_switch_optimization = SwitchOptimizationValues[ui->SwitchOptimizationCombo->currentIndex()];
    Controller->SetKeyboardSwitchOptimization(desired_switch_optimization);
    UpdateInformation();
}



void HuntsmanV2ControlWidget::on_RefreshButton_clicked()
{
    UpdateInformation();
}