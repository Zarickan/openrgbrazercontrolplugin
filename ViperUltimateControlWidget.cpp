#include "ViperUltimateControlWidget.h"
#include "ui_ViperUltimateControlWidget.h"

#include "RazerController.h"
#include "common.h"

static QString PollingRateNames[] = {
    "125 HZ",
    "500 HZ",
    "1000 HZ",
};
static unsigned char PollingRateValues[] = {
    RAZER_MOUSE_POLLING_RATE_125HZ,
    RAZER_MOUSE_POLLING_RATE_500HZ,
    RAZER_MOUSE_POLLING_RATE_1000HZ,
};

static QString WirelessPowerSavingNames[] = {
        "1 Minute",
        "2 Minutes",
        "3 Minutes",
        "4 Minutes",
        "5 Minutes",
        "6 Minutes",
        "7 Minutes",
        "8 Minutes",
        "9 Minutes",
        "10 Minutes",
        "11 Minutes",
        "12 Minutes",
        "13 Minutes",
        "14 Minutes",
        "15 Minutes",
};
static unsigned int WirelessPowerSavingValues[] = {
        1 * 60,
        2 * 60,
        3 * 60,
        4 * 60,
        5 * 60,
        6 * 60,
        7 * 60,
        8 * 60,
        9 * 60,
        10 * 60,
        11 * 60,
        12 * 60,
        13 * 60,
        14 * 60,
        15 * 60,
};

static QString SmartTrackingModeNames[] = {
        "Normal",
        "Manual Calibration",
};
static unsigned char SmartTrackingModeValues[] = {
        1,
        3,
};

static QString SmartTrackingOffsetNames[] = {
        "1mm",
        "2mm",
        "3mm",
};
static unsigned char SmartTrackingOffsetValues[] = {
        0,
        1,
        2,
};

static QString SensitivityStageNames[] = {
        "1 stage",
        "2 stages",
        "3 stages",
        "4 stages",
        "5 stages",
};

static QString ActiveStageNames[] = {
        "First stage (Red)",
        "Second stage (Green)",
        "Third stage (Blue)",
        "Fourth stage (Cyan)",
        "Fifth stage (Yellow)",
};

void ViperUltimateControlWidget::UpdateInformation() {
    ready = false;

    ui->LeftHandedCheck->setChecked(Controller->GetMouseLeftHandedMode());

    // Polling Rate
    unsigned char polling_rate = Controller->GetMousePollingRate();
    for(int i = 0; i < ARRAYSIZE(PollingRateValues); ++i) {
        if(polling_rate == PollingRateValues[i]) {
            ui->PollingRateCombo->setCurrentIndex(i);
            break;
        }
    }

    // Wireless power saving
    unsigned int power_saving_time = Controller->GetWirelessPowerSavingTime();
    for(int i = 0; i < ARRAYSIZE(WirelessPowerSavingValues); ++i) {
        if(power_saving_time == WirelessPowerSavingValues[i]) {
            ui->PowerSavingCombo->setCurrentIndex(i);
            break;
        }
    }

    // Battery Level
    unsigned short battery_level = Controller->GetBatteryLevel();
    float battery_percentage = ((float) battery_level / 255.0f) * 100.0f;
    bool is_charging = Controller->IsCharging();

    char battery_level_string[25];
    sprintf(battery_level_string, "%.2f%%, %s", battery_percentage, is_charging ? "charging" : "not charging");
    ui->BatteryValue->setText(QString::fromUtf8(battery_level_string));

    // Smart Tracking
    unsigned char smart_tracking_offset;
    unsigned char smart_tracking_mode = Controller->GetSmartTrackingMode(&smart_tracking_offset);
    if(smart_tracking_mode == 1 || smart_tracking_mode == 2) {
        ui->SmartTrackingModeCombo->setCurrentIndex(0);
    } else if (smart_tracking_mode == 3 || smart_tracking_mode == 4) {
        ui->SmartTrackingModeCombo->setCurrentIndex(1);
    }

    // Smart Tracking: Asymmetric cut-off
    bool is_asymmetric_cutoff_enabled = smart_tracking_mode == 2 || smart_tracking_mode == 4;
    if(is_asymmetric_cutoff_enabled) {
        ui->AsymmetricCutOffCheck->setCheckState(Qt::Checked);
    } else {
        ui->AsymmetricCutOffCheck->setCheckState(Qt::Unchecked);
    }

    // Smart tracking: Offset
    for(int i = 0; i < ARRAYSIZE(SmartTrackingOffsetValues); ++i) {
        if(smart_tracking_offset == SmartTrackingOffsetValues[i]) {
            ui->SmartTrackingOffsetCombo->setCurrentIndex(i);
            break;
        }
    }

    // X/Y Sensitivity
    unsigned short sensitivityX, sensitivityY;
    Controller->GetSensitivity(&sensitivityX, &sensitivityY);
    ui->SensitivityX->setValue(sensitivityX);
    ui->SensitivityY->setValue(sensitivityY);

    // Sensitivity Stages: Number of stages / active stage
    razer_sensitivity_stage_data stages;
    Controller->GetSensitivityStages(&stages);

    ui->SensitivityStagesCombo->setCurrentIndex(stages.number_of_stages - 1);

    ui->ActiveStageCombo->clear();
    for(unsigned int i = 0; i < stages.number_of_stages; ++i) {
        ui->ActiveStageCombo->addItem(ActiveStageNames[i]);
    }
    ui->ActiveStageCombo->setCurrentIndex(stages.active_stage - 1);

    // Sensitivity Stages: Individual stages
    ui->SensitivityStage1X->setValue(stages.x[0]);
    ui->SensitivityStage1Y->setValue(stages.y[0]);

    ui->SensitivityStage2X->setValue(stages.x[1]);
    ui->SensitivityStage2Y->setValue(stages.y[1]);
    ui->SensitivityStage2X->setHidden(stages.number_of_stages < 2);
    ui->SensitivityStage2Y->setHidden(stages.number_of_stages < 2);
    ui->SensitivityStage2XLabel->setHidden(stages.number_of_stages < 2);
    ui->SensitivityStage2YLabel->setHidden(stages.number_of_stages < 2);

    ui->SensitivityStage3X->setValue(stages.x[2]);
    ui->SensitivityStage3Y->setValue(stages.y[2]);
    ui->SensitivityStage3X->setHidden(stages.number_of_stages < 3);
    ui->SensitivityStage3Y->setHidden(stages.number_of_stages < 3);
    ui->SensitivityStage3XLabel->setHidden(stages.number_of_stages < 3);
    ui->SensitivityStage3YLabel->setHidden(stages.number_of_stages < 3);

    ui->SensitivityStage4X->setValue(stages.x[3]);
    ui->SensitivityStage4Y->setValue(stages.y[3]);
    ui->SensitivityStage4X->setHidden(stages.number_of_stages < 4);
    ui->SensitivityStage4Y->setHidden(stages.number_of_stages < 4);
    ui->SensitivityStage4XLabel->setHidden(stages.number_of_stages < 4);
    ui->SensitivityStage4YLabel->setHidden(stages.number_of_stages < 4);

    ui->SensitivityStage5X->setValue(stages.x[4]);
    ui->SensitivityStage5Y->setValue(stages.y[4]);
    ui->SensitivityStage5X->setHidden(stages.number_of_stages < 5);
    ui->SensitivityStage5Y->setHidden(stages.number_of_stages < 5);
    ui->SensitivityStage5XLabel->setHidden(stages.number_of_stages < 5);
    ui->SensitivityStage5YLabel->setHidden(stages.number_of_stages < 5);

    ready = true;
}

ViperUltimateControlWidget::ViperUltimateControlWidget(QWidget *parent, RazerController *controller) :
    QWidget(parent),
    ui(new Ui::ViperUltimateControlWidget)
{
    Controller = controller;

    ui->setupUi(this);
    ui->NameValue->setText(QString::fromStdString(controller->GetName()));

    // Polling rates
    for(const auto& polling_rate : PollingRateNames) {
        ui->PollingRateCombo->addItem(polling_rate);
    }

    // Wireless power saving
    for(const auto& power_saving_time : WirelessPowerSavingNames) {
        ui->PowerSavingCombo->addItem(power_saving_time);
    }

    // Smart Tracking
    for(const auto& smart_tracking_mode : SmartTrackingModeNames) {
        ui->SmartTrackingModeCombo->addItem(smart_tracking_mode);
    }

    // Smart Tracking: Offset
    for(const auto& smart_tracking_offset : SmartTrackingOffsetNames) {
        ui->SmartTrackingOffsetCombo->addItem(smart_tracking_offset);
    }

    // X/Y Sensitivity
    ui->SensitivityX->setRange(100, 20000);
    ui->SensitivityX->setSingleStep(100);
    ui->SensitivityY->setRange(100, 20000);
    ui->SensitivityY->setSingleStep(100);

    // Sensitivity Stages
    for(const auto& sensitivity_stage : SensitivityStageNames) {
        ui->SensitivityStagesCombo->addItem(sensitivity_stage);
    }

    ui->SensitivityStage1X->setRange(100, 20000);
    ui->SensitivityStage1X->setSingleStep(100);
    ui->SensitivityStage1Y->setRange(100, 20000);
    ui->SensitivityStage1Y->setSingleStep(100);

    ui->SensitivityStage2X->setRange(100, 20000);
    ui->SensitivityStage2X->setSingleStep(100);
    ui->SensitivityStage2Y->setRange(100, 20000);
    ui->SensitivityStage2Y->setSingleStep(100);

    ui->SensitivityStage3X->setRange(100, 20000);
    ui->SensitivityStage3X->setSingleStep(100);
    ui->SensitivityStage3Y->setRange(100, 20000);
    ui->SensitivityStage3Y->setSingleStep(100);

    ui->SensitivityStage4X->setRange(100, 20000);
    ui->SensitivityStage4X->setSingleStep(100);
    ui->SensitivityStage4Y->setRange(100, 20000);
    ui->SensitivityStage4Y->setSingleStep(100);

    ui->SensitivityStage5X->setRange(100, 20000);
    ui->SensitivityStage5X->setSingleStep(100);
    ui->SensitivityStage5Y->setRange(100, 20000);
    ui->SensitivityStage5Y->setSingleStep(100);

    UpdateInformation();
    ready = true;
}

ViperUltimateControlWidget::~ViperUltimateControlWidget()
{
    delete ui;
}

void ViperUltimateControlWidget::on_LeftHandedCheck_clicked()
{
    if(!ready) return;

    Controller->SetMouseLeftHandedMode(ui->LeftHandedCheck->isChecked());
    UpdateInformation();
}

void ViperUltimateControlWidget::on_PollingRateCombo_currentIndexChanged()
{
    if(!ready) return;

    unsigned char desired_polling_rate = PollingRateValues[ui->PollingRateCombo->currentIndex()];
    Controller->SetMousePollingRate(desired_polling_rate);
    UpdateInformation();
}

void ViperUltimateControlWidget::on_PowerSavingCombo_currentIndexChanged()
{
    if(!ready) return;

    unsigned int power_saving_time = WirelessPowerSavingValues[ui->PowerSavingCombo->currentIndex()];
    Controller->SetWirelessPowerSavingTime(power_saving_time);
    UpdateInformation();
}

void ViperUltimateControlWidget::UpdateSmartTracking()
{
    if(!ready) return;

    unsigned char desired_offset = SmartTrackingOffsetValues[ui->SmartTrackingOffsetCombo->currentIndex()];
    unsigned char desired_mode = SmartTrackingModeValues[ui->SmartTrackingModeCombo->currentIndex()];

    // Mode is manual calibration (with or without asymmetric cut off)
    if(desired_mode == 2) {
        Controller->SetSmartTrackingSurface(1);
        Controller->SetSmartTrackingSurface(1);
    } else {
        Controller->SetSmartTrackingSurface(0);
        Controller->SetSmartTrackingSurface(0);
    }

    if (ui->AsymmetricCutOffCheck->isChecked()) {
        desired_mode += 1;
    }
    Controller->SetSmartTrackingMode(desired_mode, desired_offset);

    UpdateInformation();
}
void ViperUltimateControlWidget::on_SmartTrackingModeCombo_currentIndexChanged()
{
    UpdateSmartTracking();
}
void ViperUltimateControlWidget::on_AsymmetricCutOffCheck_clicked()
{
    UpdateSmartTracking();
}
void ViperUltimateControlWidget::on_SmartTrackingOffsetCombo_currentIndexChanged()
{
    UpdateSmartTracking();
}

void ViperUltimateControlWidget::on_UpdateSensitivityButton_clicked()
{
    if(!ready) return;

    unsigned short x = ui->SensitivityX->value();
    unsigned short y = ui->SensitivityY->value();
    Controller->SetSensitivity(x, y);

    UpdateInformation();
}

void ViperUltimateControlWidget::UpdateSensitivityStages()
{
    if(!ready) return;

    razer_sensitivity_stage_data stages;

    stages.number_of_stages = ui->SensitivityStagesCombo->currentIndex() + 1;
    stages.active_stage = ui->ActiveStageCombo->currentIndex() + 1;

    stages.x[0] = ui->SensitivityStage1X->value();
    stages.y[0] = ui->SensitivityStage1Y->value();

    stages.x[1] = ui->SensitivityStage2X->value();
    stages.y[1] = ui->SensitivityStage2Y->value();

    stages.x[2] = ui->SensitivityStage3X->value();
    stages.y[2] = ui->SensitivityStage3Y->value();

    stages.x[3] = ui->SensitivityStage4X->value();
    stages.y[3] = ui->SensitivityStage4Y->value();

    stages.x[4] = ui->SensitivityStage5X->value();
    stages.y[4] = ui->SensitivityStage5Y->value();

    Controller->SetSensitivityStages(&stages);

    UpdateInformation();
}
void ViperUltimateControlWidget::on_SensitivityStagesCombo_currentIndexChanged()
{
    UpdateSensitivityStages();
}
void ViperUltimateControlWidget::on_ActiveStageCombo_currentIndexChanged()
{
    UpdateSensitivityStages();
}
void ViperUltimateControlWidget::on_UpdateSensitivityStagesButton_clicked()
{
    UpdateSensitivityStages();
}


void ViperUltimateControlWidget::on_RefreshButton_clicked()
{
    UpdateInformation();
}