#pragma once

#include <QWidget>

#include "RazerController.h"

namespace Ui {
class ViperUltimateControlWidget;
}

class ViperUltimateControlWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ViperUltimateControlWidget(QWidget *parent, RazerController *controller);
    ~ViperUltimateControlWidget();

private slots:
    void on_LeftHandedCheck_clicked();
    void on_PollingRateCombo_currentIndexChanged();
    void on_PowerSavingCombo_currentIndexChanged();
    void on_SmartTrackingModeCombo_currentIndexChanged();
    void on_AsymmetricCutOffCheck_clicked();
    void on_SmartTrackingOffsetCombo_currentIndexChanged();
    void on_SensitivityStagesCombo_currentIndexChanged();
    void on_ActiveStageCombo_currentIndexChanged();
    void on_UpdateSensitivityButton_clicked();
    void on_UpdateSensitivityStagesButton_clicked();

    void on_RefreshButton_clicked();

private:
    void UpdateInformation();

    void UpdateSmartTracking();
    void UpdateSensitivityStages();

    Ui::ViperUltimateControlWidget *ui;
    RazerController *Controller;
    bool ready = false;
};