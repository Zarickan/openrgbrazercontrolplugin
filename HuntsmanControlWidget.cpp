#include "HuntsmanControlWidget.h"
#include "ui_HuntsmanControlWidget.h"

#include "RazerController.h"
#include "common.h"

void HuntsmanControlWidget::UpdateInformation() {
    ready = false;

    ui->GamingModeCheck->setChecked(Controller->GetKeyboardGamingMode());

    ready = true;
}

HuntsmanControlWidget::HuntsmanControlWidget(QWidget *parent, RazerController *controller) :
    QWidget(parent),
    ui(new Ui::HuntsmanControlWidget)
{
    Controller = controller;

    ui->setupUi(this);
    ui->NameValue->setText(QString::fromStdString(controller->GetName()));

    UpdateInformation();
    ready = true;
}

HuntsmanControlWidget::~HuntsmanControlWidget()
{
    delete ui;
}

void HuntsmanControlWidget::on_GamingModeCheck_clicked()
{
    if(!ready) return;

    Controller->SetKeyboardGamingMode(ui->GamingModeCheck->isChecked());
    UpdateInformation();
}



void HuntsmanControlWidget::on_RefreshButton_clicked()
{
    UpdateInformation();
}