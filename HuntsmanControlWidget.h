#pragma once

#include <QWidget>

#include "RazerController.h"

namespace Ui {
class HuntsmanControlWidget;
}

class HuntsmanControlWidget : public QWidget
{
    Q_OBJECT

public:
    explicit HuntsmanControlWidget(QWidget *parent, RazerController *controller);
    ~HuntsmanControlWidget();

private slots:
    void on_GamingModeCheck_clicked();

    void on_RefreshButton_clicked();

private:
    void UpdateInformation();

    Ui::HuntsmanControlWidget *ui;
    RazerController *Controller;
    bool ready = false;
};