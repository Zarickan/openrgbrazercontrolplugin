#include "BlackWidowV3MiniControlWidget.h"
#include "ui_BlackWidowV3MiniControlWidget.h"

#include "RazerController.h"
#include "common.h"

static QString WirelessPowerSavingNames[] = {
        "15 Minutes",
        "16 Minutes",
        "17 Minutes",
        "18 Minutes",
        "19 Minutes",
        "20 Minutes",
        "21 Minutes",
        "22 Minutes",
        "23 Minutes",
        "24 Minutes",
        "25 Minutes",
        "26 Minutes",
        "27 Minutes",
        "28 Minutes",
        "29 Minutes",
        "30 Minutes"
};
static unsigned int WirelessPowerSavingValues[] = {
        15 * 60,
        16 * 60,
        17 * 60,
        18 * 60,
        19 * 60,
        20 * 60,
        21 * 60,
        22 * 60,
        23 * 60,
        24 * 60,
        25 * 60,
        26 * 60,
        27 * 60,
        28 * 60,
        29 * 60,
        30 * 60
};

static QString DimLightingAfterNames[] = {
        "Disabled",
        "1 Minutes",
        "2 Minutes",
        "3 Minutes",
        "4 Minutes",
        "5 Minutes",
        "6 Minutes",
        "7 Minutes",
        "8 Minutes",
        "9 Minutes",
        "10 Minutes",
        "11 Minutes",
        "12 Minutes",
        "13 Minutes",
        "14 Minutes",
        "15 Minutes"
};
static unsigned int DimLightingAfterValues[] = {
        0,
        1 * 60,
        2 * 60,
        3 * 60,
        4 * 60,
        5 * 60,
        6 * 60,
        7 * 60,
        8 * 60,
        9 * 60,
        10 * 60,
        11 * 60,
        12 * 60,
        13 * 60,
        14 * 60,
        15 * 60
};

void BlackWidowV3MiniControlWidget::UpdateInformation() {
    ui->GamingModeCheck->setChecked(Controller->GetKeyboardGamingMode());

    // Wireless power saving
    unsigned int power_saving_time = Controller->GetWirelessPowerSavingTime();
    for(int i = 0; i < ARRAYSIZE(WirelessPowerSavingValues); ++i) {
        if(power_saving_time == WirelessPowerSavingValues[i]) {
            ui->PowerSavingCombo->setCurrentIndex(i);
            break;
        }
    }

    // Dim lighting after
    unsigned int dim_lighting_after = Controller->GetDimLightingConfiguration();
    for(int i = 0; i < ARRAYSIZE(DimLightingAfterValues); ++i) {
        if(dim_lighting_after == DimLightingAfterValues[i]) {
            ui->DimLightingCombo->setCurrentIndex(i);
            break;
        }
    }

    // Battery Level
    unsigned short battery_level = Controller->GetBatteryLevel();
    float battery_percentage = ((float) battery_level / 255.0f) * 100.0f;
    bool is_charging = Controller->IsCharging();

    char battery_level_string[25];
    sprintf(battery_level_string, "%.2f%%, %s", battery_percentage, is_charging ? "charging" : "not charging");
    ui->BatteryValue->setText(QString::fromUtf8(battery_level_string));
}



BlackWidowV3MiniControlWidget::BlackWidowV3MiniControlWidget(QWidget *parent, RazerController *controller) :
    QWidget(parent),
    ui(new Ui::BlackWidowV3MiniControlWidget)
{
    Controller = controller;

    ui->setupUi(this);
    ui->NameValue->setText(QString::fromStdString(controller->GetName()));

    // Wireless power saving
    for(const auto& power_saving_time : WirelessPowerSavingNames) {
        ui->PowerSavingCombo->addItem(power_saving_time);
    }

    // Dim lighting after
    for(const auto& dim_lighting_after : DimLightingAfterNames) {
        ui->DimLightingCombo->addItem(dim_lighting_after);
    }

    UpdateInformation();
    ready = true;
}

BlackWidowV3MiniControlWidget::~BlackWidowV3MiniControlWidget()
{
    delete ui;
}



void BlackWidowV3MiniControlWidget::on_GamingModeCheck_clicked()
{
    if(!ready) return;

    Controller->SetKeyboardGamingMode(ui->GamingModeCheck->isChecked());
    UpdateInformation();
}

void BlackWidowV3MiniControlWidget::on_PowerSavingCombo_currentIndexChanged()
{
    if(!ready) return;

    unsigned int power_saving_time = WirelessPowerSavingValues[ui->PowerSavingCombo->currentIndex()];
    Controller->SetWirelessPowerSavingTime(power_saving_time);
    UpdateInformation();
}

void BlackWidowV3MiniControlWidget::on_DimLightingCombo_currentIndexChanged() {
    if(!ready) return;

    unsigned int dim_lighting_after = DimLightingAfterValues[ui->DimLightingCombo->currentIndex()];
    Controller->SetDimLightingConfiguration(dim_lighting_after);
    UpdateInformation();
}



void BlackWidowV3MiniControlWidget::on_RefreshButton_clicked()
{
    UpdateInformation();
}