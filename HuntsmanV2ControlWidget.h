#pragma once

#include <QWidget>

#include "RazerController.h"

namespace Ui {
class HuntsmanV2ControlWidget;
}

class HuntsmanV2ControlWidget : public QWidget
{
    Q_OBJECT

public:
    explicit HuntsmanV2ControlWidget(QWidget *parent, RazerController *controller);
    ~HuntsmanV2ControlWidget();

private slots:
    void on_GamingModeCheck_clicked();
    void on_PollingRateCombo_currentIndexChanged();
    void on_SwitchOptimizationCombo_currentIndexChanged();

    void on_RefreshButton_clicked();

private:
    void UpdateInformation();

    Ui::HuntsmanV2ControlWidget *ui;
    RazerController *Controller;
    bool ready = false;
};