#pragma once

#include <QWidget>

#include "RazerController.h"

namespace Ui {
class BlackWidowV3MiniControlWidget;
}

class BlackWidowV3MiniControlWidget : public QWidget
{
    Q_OBJECT

public:
    explicit BlackWidowV3MiniControlWidget(QWidget *parent, RazerController *controller);
    ~BlackWidowV3MiniControlWidget();

private slots:
    void on_GamingModeCheck_clicked();
    void on_PowerSavingCombo_currentIndexChanged();
    void on_DimLightingCombo_currentIndexChanged();

    void on_RefreshButton_clicked();

private:
    void UpdateInformation();

    Ui::BlackWidowV3MiniControlWidget *ui;
    RazerController *Controller;
    bool ready = false;
};