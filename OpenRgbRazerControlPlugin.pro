QT +=                  \
    gui                \
    widgets            \
    core               \

win32:CONFIG += QTPLUGIN

TEMPLATE = lib
DEFINES += ORGBRAZEREXTRASPLUGIN_LIBRARY

win32:CONFIG += c++17

unix:!macx {
  QMAKE_CXXFLAGS += -std=c++17
}

#-----------------------------------------------------------------------------------------------#
# Application Configuration                                                                     #
#-----------------------------------------------------------------------------------------------#
PLUGIN_VERSION     = 0.1

#-----------------------------------------------------------------------------------------------#
# Automatically generated build information                                                     #
#-----------------------------------------------------------------------------------------------#
win32:BUILDDATE = $$system(date /t)
unix:BUILDDATE  = $$system(date -R -d "@${SOURCE_DATE_EPOCH:-$(date +%s)}")
GIT_COMMIT_ID   = $$system(git --git-dir $$_PRO_FILE_PWD_/.git --work-tree $$_PRO_FILE_PWD_ rev-parse HEAD)
GIT_COMMIT_DATE = $$system(git --git-dir $$_PRO_FILE_PWD_/.git --work-tree $$_PRO_FILE_PWD_ show -s --format=%ci HEAD)
GIT_BRANCH      = $$system(git --git-dir $$_PRO_FILE_PWD_/.git --work-tree $$_PRO_FILE_PWD_ rev-parse --abbrev-ref HEAD)

#-----------------------------------------------------------------------------------------------#
# Inject vars in defines                                                                        #
#-----------------------------------------------------------------------------------------------#
DEFINES +=                                                                                      \
    VERSION_STRING=\\"\"\"$$PLUGIN_VERSION\\"\"\"                                               \
    BUILDDATE_STRING=\\"\"\"$$BUILDDATE\\"\"\"                                                  \
    GIT_COMMIT_ID=\\"\"\"$$GIT_COMMIT_ID\\"\"\"                                                 \
    GIT_COMMIT_DATE=\\"\"\"$$GIT_COMMIT_DATE\\"\"\"                                             \
    GIT_BRANCH=\\"\"\"$$GIT_BRANCH\\"\"\"                                                       \
    LATEST_BUILD_URL=\\"\"\"$$LATEST_BUILD_URL\\"\"\"                                           \

#-------------------------------------------------------------------#
# Includes                                                          #
#-------------------------------------------------------------------#
INCLUDEPATH +=                                                                                  \
    OpenRGB/                                                                                    \
    OpenRGB/i2c_smbus                                                                           \
    OpenRGB/RGBController                                                                       \
    OpenRGB/net_port                                                                            \
    OpenRGB/dependencies/json                                                                   \
    OpenRGB/dependencies/hidapi/hidapi                                                          \
    OpenRGB/Controllers/RazerController                                                         \

HEADERS +=                                                                                      \
    OpenRGB/dependencies/hidapi/hidapi/hidapi.h                                                 \
    OpenRGB/Controllers/RazerController/RazerDevices.h                                          \
    OpenRGB/Controllers/RazerController/RazerController.h                                       \
    OpenRGB/OpenRGBPluginInterface.h                                                            \
    OpenRGB/ResourceManager.h                                                                   \
    common.h \
    OpenRgbRazerControlPlugin.h \
    HuntsmanV2ControlWidget.h \
    HuntsmanControlWidget.h \
    ViperUltimateControlWidget.h \
    BlackWidowV3MiniControlWidget.h

SOURCES +=                                                                                      \
    OpenRGB/Controllers/RazerController/RazerController.cpp                                     \
    OpenRgbRazerControlPlugin.cpp \
    HuntsmanV2ControlWidget.cpp \
    HuntsmanControlWidget.cpp \
    ViperUltimateControlWidget.cpp \
    BlackWidowV3MiniControlWidget.cpp

#-----------------------------------------------------------------------------------------------#
# Windows-specific Configuration                                                                #
#-----------------------------------------------------------------------------------------------#
win32:INCLUDEPATH +=                                                                            \
    OpenRGB/dependencies/libusb-1.0.22/include                                                  \
    OpenRGB/dependencies/hidapi                                                                 \


#-------------------------------------------------------------------#
# Windows GitLab CI Configuration                                   #
#-------------------------------------------------------------------#
win32:CONFIG(debug, debug|release) {
    win32:DESTDIR = debug
}

win32:CONFIG(release, debug|release) {
    win32:DESTDIR = release
}

win32:OBJECTS_DIR = _intermediate_$$DESTDIR/.obj
win32:MOC_DIR     = _intermediate_$$DESTDIR/.moc
win32:RCC_DIR     = _intermediate_$$DESTDIR/.qrc
win32:UI_DIR      = _intermediate_$$DESTDIR/.ui

win32:contains(QMAKE_TARGET.arch, x86_64) {
    LIBS +=                                                             \
        -lws2_32                                                        \
        -lole32                                                         \
        -L"$$PWD/OpenRGB/dependencies/libusb-1.0.22/MS64/dll" -llibusb-1.0      \
        -L"$$PWD/OpenRGB/dependencies/hidapi-win/x64/" -lhidapi
}

win32:contains(QMAKE_TARGET.arch, x86) {
    LIBS +=                                                             \
        -lws2_32                                                        \
        -lole32                                                         \
        -L"$$PWD/OpenRGB/dependencies/libusb-1.0.22/MS32/dll" -llibusb-1.0      \
        -L"$$PWD/OpenRGB/dependencies/hidapi-win/x86/" -lhidapi
}

win32:DEFINES +=                                                        \
    _MBCS                                                               \
    WIN32                                                               \
    _CRT_SECURE_NO_WARNINGS                                             \
    _WINSOCK_DEPRECATED_NO_WARNINGS                                     \
    WIN32_LEAN_AND_MEAN                                                 \

#-----------------------------------------------------------------------#
# Linux-specific Configuration                                          #
#-----------------------------------------------------------------------#
unix:!macx {
}

#-----------------------------------------------------------------------#
# MacOS-specific Configuration                                          #
#-----------------------------------------------------------------------#
QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.15

macx: {
    DEFINES +=                                                                                  \
    USE_HID_USAGE                                                                               \

    QMAKE_CXXFLAGS +=                                                                           \
    -Wno-narrowing

    CONFIG += c++17

    LIBS +=                                                                                     \
    -lusb-1.0                                                                                   \
    -lhidapi                                                                                    \
}
#-------------------------------------------------------------------------------------------#
# Apple Silicon (arm64) Homebrew installs at /opt/homebrew                                  #
#-------------------------------------------------------------------------------------------#
macx:contains(QMAKE_HOST.arch, arm64) {
    INCLUDEPATH +=                                                                              \
    /opt/homebrew/include                                                                       \

    LIBS +=                                                                                     \
    -L/opt/homebrew/lib                                                                         \
}

#-------------------------------------------------------------------------------------------#
# Intel (x86_64) Homebrew installs at /usr/local/lib                                        #
#-------------------------------------------------------------------------------------------#
macx:contains(QMAKE_HOST.arch, x86_64) {
    INCLUDEPATH +=                                                                              \
    /usr/local/include                                                                          \
    /usr/local/homebrew/include                                                                 \

    LIBS +=                                                                                     \
    -L/usr/local/lib                                                                            \
    -L/usr/local/homebrew/lib                                                                   \
}

RESOURCES += \
    resources.qrc

FORMS += \
    HuntsmanV2ControlWidget.ui \
    HuntsmanControlWidget.ui \
    ViperUltimateControlWidget.ui \
    BlackWidowV3MiniControlWidget.ui


